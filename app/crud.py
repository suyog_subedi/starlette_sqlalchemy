from sqlalchemy.orm import Session
from . database import engine
from . import models,schemas

local_session = Session(bind=engine)

def get_user(db:Session,user_id:int):
    return db.query(models.User).filter(models.User.id==user_id).first()


def get_user_by_email(db:Session, email:str):
    return db.query(models.User).filter(models.User.email==email).first()

def get_users(db:Session, skip:int=0, limit:int=100):
    return db.query(models.User).offset(skip).limit(limit).all()

# ----------------------Original Code ---------------
# def create_user(db:Session, user:schemas.UserCreate):
#     fake_hashed_password = user.password +'nothashed'
#     db_user = models.User(email=user.email,password=fake_hashed_password)
#     db.add(db_user)
#     db.refresh(db_user)
#     return db_user

def create_user(db:local_session, user:schemas.UserCreate):
    fake_hashed_password = user.password +'nothashed'
    db_user = models.User(email=user.email,password=fake_hashed_password)
    db.add(db_user)
    db.refresh(db_user)
    return db_user

# ----------------------Changed Code ---------------
# def create_user(user_data):
#     new_user = models.User(**user_data)
#     try:
#         local_session.add(new_user)
#     except:
#         local_session.rollback()
#     finally:
#         local_session.close()





def get_items(db:Session, skip:int=0, limit:int=100):
    return db.query(models.Item).offset(skip).limit(limit).all()

def create_user_item(db:Session,item:schemas.ItemCreate, user_id:int):
    db_item = models.Item(**item.dict(),owner_id= user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
    