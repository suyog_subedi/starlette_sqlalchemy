
from typing import List
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.routing import Route
from starlette.exceptions import HTTPException
from sqlalchemy.orm import Session
from . import crud, models,schemas


from .database import SessionLocal,engine

models.Base.metadata.create_all(bind=engine)

local_session = Session(bind=engine)

app = Starlette()

@app.route("/",methods=["GET"])
async def homepage(request):
    return JSONResponse({'Homepage': 'Very interesting homepage'})

@app.route("/users",methods=['POST'])
def create_user(db:local_session, user:schemas.UserCreate):
    print("Inside Create User")
    SessionLocal()
   
    db_user = crud.get_user_by_email(db)
    if db_user:
        raise HTTPException(status_code=400,detail="Email already registered")
    return crud.create_user(db=db,user=user)


@app.route("/error",methods=['GET'])
async def error(request:Request):
    return JSONResponse({'Errorpage': 'This is error page'})